from django.test import TestCase
from django.urls import reverse
from django.urls import resolve
from .views import index, profile
import environ


root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')


def session_login(self, username, password):
    return self.client.post(reverse('lab-9:auth_login'), {'username': username, 'password': password})


def cookie_login(self, username, password):
    return self.client.post(reverse('lab-9:cookie_auth_login'), {'username': username, 'password': password})


class Lab9UnitTest(TestCase):

    def test_lab_9_session_login_url_is_exist(self):
        response = self.client.get(reverse('lab-9:index'))
        self.assertEqual(response.status_code, 200)

    def test_lab_9_cookie_login_url_is_exist(self):
        response = self.client.get(reverse('lab-9:cookie_login'))
        self.assertEqual(response.status_code, 200)

    def test_lab_9_using_index_func(self):
        found = resolve(reverse('lab-9:index'))
        self.assertEqual(found.func, index)

    def test_lab_9_profile_using_profile_func(self):
        found = resolve(reverse('lab-9:profile'))
        self.assertEqual(found.func, profile)

    def test_lab_9_session_login_page_already_login(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        response = self.client.get(reverse('lab-9:index'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('lab-9:profile'))

    def test_lab_9_login_session_success(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        session = self.client.session
        self.assertEqual(session['user_login'], env("SSO_USERNAME"))

    def test_lab_9_login_session_fail(self):
        session_login(self, "asdf", "asdf")
        session = self.client.session
        self.assertEqual(session.get('user_login'), None)

    def test_lab_9_profile_show_profile(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        response = self.client.get(reverse('lab-9:profile'))
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertIn(env("SSO_USERNAME"), html_response)

    def test_lab_9_profile_not_login(self):
        response = self.client.get(reverse('lab-9:profile'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('lab-9:index'))

    def test_lab_9_logout(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        response = self.client.post(reverse('lab-9:auth_logout'))
        session = self.client.session
        self.assertEqual(session.get('user_login'), None)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('lab-9:index'))

    def test_lab_9_cookie_login(self):
        cookie_login(self, "admin", "admin")
        cookies = self.client.cookies
        self.assertEqual(cookies['user_login'].value, 'admin')
        self.assertEqual(cookies['user_password'].value, 'admin')

    def test_lab_9_cookie_login_fail(self):
        response = cookie_login(self, "admin", "heho")
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('lab-9:cookie_login'))

    def test_lab_9_cookie_login_page_already_login(self):
        cookie_login(self, "admin", "admin")
        response = self.client.get(reverse('lab-9:cookie_login'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('lab-9:cookie_profile'))

    def test_lab_9_get_cookie_auth_login(self):
        response = self.client.get(reverse('lab-9:cookie_auth_login'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('lab-9:cookie_login'))

    def test_lab_9_cookie_profile_not_login(self):
        response = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('lab-9:cookie_login'))

    def test_lab_9_cookie_clear(self):
        cookie_login(self, "admin", "admin")
        response = self.client.get(reverse('lab-9:cookie_clear'))
        cookies = self.client.cookies
        self.assertEqual(response.status_code, 302)
        self.assertEqual(cookies["user_login"].value, '')

    def test_lab_9_add_session_item(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': '124'}))
        response = self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': '123'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('lab-9:profile'))
        session = self.client.session
        self.assertIn('123', session.get('drones'))
        self.assertEqual(2, len(session.get('drones')))

    def test_lab_9_del_session_item(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': '123'}))
        self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': '124'}))
        response = self.client.get(reverse('lab-9:del_session_item', kwargs={'key': 'drones', 'id': '123'}))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('lab-9:profile'))
        session = self.client.session
        self.assertNotIn('123', session.get('drones'))
        self.assertEqual(1, len(session.get('drones')))

    def test_lab_9_clear_session_item(self):
        session_login(self, env("SSO_USERNAME"), env("SSO_PASSWORD"))
        self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': '123'}))
        self.client.get(reverse('lab-9:add_session_item', kwargs={'key': 'drones', 'id': '124'}))
        response = self.client.get(reverse('lab-9:clear_session_item', kwargs={'key': 'drones'}))
        self.assertEqual(response.status_code, 302)
        session = self.client.session
        self.assertEqual(session.get('drones'), None)
