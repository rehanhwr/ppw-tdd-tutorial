
 // FB initiation function
  window.fbAsyncInit = () => {
    FB.init({
      appId      : '132719874030769',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });

    //cek status login
    FB.getLoginStatus(function(response){
      render(response.status==="connected");
    });
  };

  // Call init facebook. default dari facebook
  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

  const render = loginFlag => {
    if (loginFlag) {
      getUserData(user => {
        $('#lab8').html(
          '<div class="profile">' +
            '<img class="cover" src="' + user.cover.source + '" alt="cover" />' +
            '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
            '<div class="data">' +
              '<h1>' + user.name + '</h1>' +
              '<h2>' + user.about + '</h2>' +
              '<h3>' + user.email + ' - ' + user.gender + '</h3>' +
            '</div>' +
          '</div>' +
          '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
          '<button class="postStatus" onclick="postStatus()">Post ke Facebook</button>' +
          '<button class="logout" onclick="facebookLogout()">Logout</button>'
        );
      });
      getUserFeed(feed => {
        feed.data.map(value => {
          if (value.message && value.story) {
            $('#lab8feeds').append(
              '<div class="feed">' +
                '<h1>' + value.message + '</h1>' +
                '<h2>' + value.story + '</h2>' +
              '</div>'
            );
          } else if (value.message) {
            $('#lab8feeds').append(
              '<div class="feed">' +
                '<h1>' + value.message + '</h1>' +
              '</div>'
            );
          } else if (value.story) {
            $('#lab8feeds').append(
              '<div class="feed">' +
                '<h2>' + value.story + '</h2>' +
              '</div>'
            );
          }
        });
      });
    } else {
      $('#lab8').html('<button class="login" onclick="facebookLogin()">Login</button>');
    }
  };

  const facebookLogin = () => {
     FB.login(function(response){
      if (response.authResponse) {
        render(true);
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', function(response) {
          console.log('Good to see you, ' + response.name + '.');
        });
      } else {
        console.log('User cancelled login or did not fully authorize.');
      }
     },{scope:'email,public_profile,user_posts,publish_actions,user_about_me,'});
  };

  const facebookLogout = () => {
    FB.logout(response => {
      render(false);
    })
  };

  const getUserData = (callback) => {
    FB.api('/me?fields=cover,picture.type(large),name,about,email,gender', 'GET', (response) => {
      console.log(response);
      callback(response);
    });
  };

  const getUserFeed = (callback) => {
    FB.api('/me/feed', 'GET', (response) => {
      console.log(response);
      callback(response);
    });
  };

  const postFeed = (message) => {
    FB.api('me/feed', 'POST', {"message": message},(response) => {
      render(true);
    });
  };

  const postStatus = () => {
    const message = $('#postInput').val();
    postFeed(message);
  };




