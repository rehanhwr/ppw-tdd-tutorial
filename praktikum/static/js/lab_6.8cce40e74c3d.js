//Chatbox
var isSend = true;
function sendMessage(msg){
	return "<div class='msg-send'>" + msg + "</div>";
}

function receiveMessage(msg){
	return "<div class='msg-receive'>" + msg + "</div>";
}

function addMessage(msg){
	if (isSend){
		$('.msg-insert').append(sendMessage(msg));
	}
	else {
		$('.msg-insert').append(receiveMessage(msg));
	}
	isSend = !isSend;
}

// Calculator
var erase = false;

var go = function(x) {
	var print = document.getElementById('print');
	if (erase){
		print.value = "";
		erase = false;
	}
	if (x === 'ac') {
		print.value = "";
	} else if (x === 'eval') {
		print.value = Math.round(evil(print.value) * 10000) / 10000;
		erase = true;
	} else {
		if (x === 'log') {
			var val = Math.round(evil(print.value) * 10000) / 10000; 
			print.value = Math.log(val);
		}
		else if (x === 'sin') {
			var degrees = Math.round(evil(print.value) * 10000) / 10000; 
			var radians = degrees*Math.PI/180;
			print.value = Math.sin(radians);

		}
		else if (x === 'tan'){
			var degrees = Math.round(evil(print.value) * 10000) / 10000; 
			var radians = degrees*Math.PI/180;
			print.value = Math.tan(radians);

		}
		else print.value += x;
	}
};

function evil(fn) {
	return new Function('return ' + fn)();
}



//Theme change
function applyTheme(id){
	var selected_item = JSON.parse(localStorage.getItem('themes'))[id];
	$("body").css("background-color", selected_item.bcgColor);
	localStorage.setItem('selected_themes', JSON.stringify(selected_item));
}

function initBackgroundColor() {
	applyTheme(3);
}

$(document).ready(function() {
	$("textarea").keyup(function(e) {
		var code = e.keyCode ? e.keyCode : e.which;
   		if (code == 13) {  // Enter keycode
   			$("textarea").val("");
		}
	});

	$("textarea").keydown(function(e) {
		var code = e.keyCode ? e.keyCode : e.which;
   		if (code == 13) {  // Enter keycode
   			addMessage($("textarea").val());
		}
	});

	var themes = 
	[{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
	{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
	{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
	{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
	{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
	{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
	{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
	{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
	{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
	{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
	{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}];
	
	localStorage.setItem('themes', JSON.stringify(themes));

	if (localStorage.getItem('selected_themes') === null) initBackgroundColor();
	
	var currentTheme = JSON.parse(localStorage.getItem('selected_themes'));
	applyTheme(currentTheme.id);

	$('.my-select').select2({
		'data' : JSON.parse(localStorage.getItem('themes')),
	});

	$('.apply-button').on('click', function(){ 
		applyTheme($('.my-select').val());
	});

});
