// Calculator
var erase = false;

var go = function(x) {
	var print = document.getElementById('print');
	if (erase){
		print.value = "";
		erase = false;
	}
	if (x === 'ac') {
		print.value = "";
	} else if (x === 'eval') {
		print.value = Math.round(evil(print.value) * 10000) / 10000;
		erase = true;
	} else {
		if (x === 'log') {
			var val = Math.round(evil(print.value) * 10000) / 10000; 
			print.value = Math.log(val);
		}
		else if (x === 'sin') {
			var degrees = Math.round(evil(print.value) * 10000) / 10000; 
			var radians = degrees*Math.PI/180;
			print.value = Math.sin(radians);

		}
		else if (x === 'tan'){
			var degrees = Math.round(evil(print.value) * 10000) / 10000; 
			var radians = degrees*Math.PI/180;
			print.value = Math.tan(radians);

		}
		else print.value += x;
	}
};

function evil(fn) {
	return new Function('return ' + fn)();
}
// END
$(document).ready(function() {
    $('.my-select').select2();
    var themes = '{"themes": [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},{"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},{"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},{"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},{"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},{"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},{"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},{"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},{"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},{"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},{"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}]}';
    
    localStorage.setItem('themes', themes);

    var selected_themes = '{"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}';
	
	localStorage.setItem('selected-items', JSON.stiringify(selected_themes));

});
